package funcionalidad.datos;

import java.util.ArrayList;

public class Docente extends Usuario{
	private ArrayList<String> areas=null;
	public Docente(String documento, String nombre, String apellido, ArrayList<String> areas) {
		super(documento, nombre, apellido);
		this.areas=areas;
	}
	public ArrayList<String> getAreas() {
		return areas;
	}
	public void setAreas(ArrayList<String> areas) {
		this.areas = areas;
	}
}

package funcionalidad.datos;


public class Asesoria {
	private Estudiante estudiante;
	private Docente docente;
	private String fecha;
	private String tematica;
	public Asesoria(Estudiante estudiante, Docente docente, String fecha, String tematica){
		this.estudiante=estudiante;
		this.docente=docente;
		this.fecha=fecha;
		this.tematica=tematica;
	}
	public Estudiante getEstudiante() {
		return estudiante;
	}
	public void setEstudiante(Estudiante estudiante) {
		this.estudiante = estudiante;
	}
	public Docente getDocente() {
		return docente;
	}
	public void setDocente(Docente docente) {
		this.docente = docente;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getTematica() {
		return tematica;
	}
	public void setTematica(String tematica) {
		this.tematica = tematica;
	}
}

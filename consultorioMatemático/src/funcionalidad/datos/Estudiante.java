package funcionalidad.datos;


public class Estudiante extends Usuario{
	private String tipoDocumento;
	private String institucion;
	private String tipoInstitucion;
	private String programa;
	public Estudiante(String documento, String nombre, String apellido,String tipoDocumento,String institucion,String tipoInstitucion,String programa){
		super(documento,nombre,apellido);
		this.tipoDocumento=tipoDocumento;
		this.institucion=institucion;
		this.tipoInstitucion=tipoInstitucion;
		this.programa=programa;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getInstitucion() {
		return institucion;
	}
	public void setInstitucion(String institucion) {
		this.institucion = institucion;
	}
	public String getTipoInstitucion() {
		return tipoInstitucion;
	}
	public void setTipoInstitucion(String tipoInstitucion) {
		this.tipoInstitucion = tipoInstitucion;
	}
	public String getPrograma() {
		return programa;
	}
	public void setPrograma(String programa) {
		this.programa = programa;
	}
}

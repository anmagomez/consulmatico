package funcionalidad.operaciones;

import java.util.ArrayList;

import almacenSecundario.AlmacenDocente;
import almacenSecundario.AlmacenEstudiante;
import funcionalidad.datos.Docente;
import funcionalidad.datos.Estudiante;


public class GestorUsuario {
	
	private AlmacenEstudiante almEstudiante;
	private AlmacenDocente almDocente;

	public GestorUsuario(){
		almEstudiante=new AlmacenEstudiante();
		almDocente=new AlmacenDocente();
	}
	public int guardarEstudiante(Estudiante estudiante) {
		return almEstudiante.guardarEstudiante(estudiante);

	}													
	public int guardarDocente(Docente docente) {
		return almDocente.guardarDocente(docente);
	}

	public Estudiante buscarEstudiante(String documento){
		return almEstudiante.consultarEstudiante(documento);
	}

	public Docente buscarDocente(String documento){
		return almDocente.consultarDocente(documento);
	}

	public Docente buscarNombreDocente(String nombre){
		ArrayList<Docente> listaDocentes=getDocentes();
		for(Docente docente:listaDocentes){
			if(docente.getNombre().equalsIgnoreCase(nombre)){
				return docente;
			}
		}
		return null;
	}

	public ArrayList<Docente> getDocentes() {
		return almDocente.listarDocentes();
	}
}

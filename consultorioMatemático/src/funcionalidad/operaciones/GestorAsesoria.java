package funcionalidad.operaciones;

import java.util.ArrayList;

import almacenSecundario.AlmacenAsesoria;
import funcionalidad.datos.Asesoria;
import funcionalidad.datos.Estudiante;
import util.Const;

public class GestorAsesoria {
	private static GestorAsesoria instance=new GestorAsesoria();
	
	private AlmacenAsesoria almAsesoria;
	
	private GestorAsesoria(){
		almAsesoria=new AlmacenAsesoria();
	}
	
	public static GestorAsesoria getInstance(){
		return instance;
	}
	
	public int guardarAsesoria(Asesoria asesoria) {
		return almAsesoria.guardarAsesoria(asesoria);
		
	}
	
	public int cantidadAsesoradosTotal(){
		ArrayList<Asesoria> asesorias = getAsesorias();
		if(asesorias==null)
			return 0;
		return asesorias.size();
	}
	private ArrayList<Asesoria> getAsesorias(){
		return almAsesoria.listarAsesorias();
	}
	
	public int cantidadAsesoradosPorDocente(String nombreDocente){
		ArrayList<Asesoria> asesorias = getAsesorias();
		if(asesorias==null)
			return 0;
		int cantidad=0;
		for(int i=0; i<asesorias.size(); i++){
			Asesoria aux = asesorias.get(i);
			if(aux.getDocente().getNombre().equalsIgnoreCase(nombreDocente))
				cantidad++;
		}
		return cantidad;
	}

	public ArrayList<Estudiante> asesoradosPorDocente(String nombreDocente) {
		ArrayList<Asesoria> asesorias = getAsesorias();
		if(asesorias==null)
			return null;
		ArrayList<Estudiante> asesorados = new ArrayList<Estudiante>();
		for(int i=0; i<asesorias.size(); i++){
			Asesoria aux = asesorias.get(i);
			if(aux.getDocente().getNombre().equalsIgnoreCase(nombreDocente))
				if(!asesorados.contains(aux.getEstudiante()))
					asesorados.add(aux.getEstudiante());
		}
		return asesorados;
	}
	
	public int asesoradosPorPeriodo(String fechaInicial, String fechaFinal){
		ArrayList<Asesoria> asesorias = getAsesorias();
		if(asesorias==null)
			return 0;
		int cantidad=0;
		
		String[] fInicial=fechaInicial.split("/");
		String[] fFinal=fechaFinal.split("/");
		for(int i=0; i<asesorias.size(); i++){
			Asesoria aux = asesorias.get(i);
			String[] fAsesoria=aux.getFecha().split("/");
			
			boolean limiteInicial=false;
			if(Integer.parseInt(fInicial[2])<=Integer.parseInt(fAsesoria[2])){
				if(Integer.parseInt(fInicial[2])==Integer.parseInt(fAsesoria[2])){
					if(Integer.parseInt(fInicial[1])<=Integer.parseInt(fAsesoria[1])){
						if(Integer.parseInt(fInicial[1])==Integer.parseInt(fAsesoria[1])){
							if(Integer.parseInt(fInicial[0])<=Integer.parseInt(fAsesoria[0])){
								limiteInicial=true;
							}
						}else
							limiteInicial=true;
					}
				}else
					limiteInicial=true;
			}
			
			boolean limiteFinal=false;
			if(Integer.parseInt(fAsesoria[2])<=Integer.parseInt(fFinal[2])){
				if(Integer.parseInt(fAsesoria[2])==Integer.parseInt(fFinal[2])){
					if(Integer.parseInt(fAsesoria[1])<=Integer.parseInt(fFinal[1])){
						if(Integer.parseInt(fAsesoria[1])==Integer.parseInt(fFinal[1])){
							if(Integer.parseInt(fAsesoria[0])<=Integer.parseInt(fFinal[0]))
								limiteFinal=true;
						}else
							limiteFinal=true;
					}
				}else
					limiteFinal=true;
			}
			if(limiteInicial&&limiteFinal)
				cantidad++;
		}
		return cantidad;
	}
	
	
}

package gui;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JLabel;

import java.awt.Rectangle;
import java.util.ArrayList;

import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JCheckBox;

import funcionalidad.datos.Docente;
import funcionalidad.datos.Estudiante;
import funcionalidad.datos.Usuario;
import util.Const;

public class FrmRegistro extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel jContentPane = null;
	private JLabel jlblNombre = null;
	private JLabel jlblApellido = null;
	private JLabel jlblDocumento = null;
	private JTextField jtxtDocumento = null;
	private JTextField jtxtNombre = null;
	private JTextField jtxtApellido = null;
	private JRadioButton jrdbTI = null;
	private JRadioButton jrdbCC = null;
	private JLabel jlblInstitucion = null;
	private JRadioButton jrdbInstitucion = null;
	private JRadioButton jrdbOtra = null;
	private JTextField jtxtOtra = null;
	private JRadioButton jrdbUniversidad = null;
	private JRadioButton jrdbColegio = null;
	private JLabel jlblPrograma = null;
	private JTextField jtxtPrograma = null;
	private JLabel jlbl = null;
	private JCheckBox jchkMatematicasOperativas = null;
	private JCheckBox jchkCalculo = null;
	private JCheckBox jchkGeometria = null;
	private JCheckBox jchkAlgebraLineal = null;
	private JCheckBox jchkAlgebraVectorial = null;
	private JCheckBox jchkFisica = null;
	private JCheckBox jchkEstadistica = null;
	private ButtonGroup bgTipoDocumento=new ButtonGroup();  //  @jve:decl-index=0:
	private ButtonGroup bgInstitucion=new ButtonGroup();  //  @jve:decl-index=0:
	private ButtonGroup bgTipoInstitucion=new ButtonGroup();  //  @jve:decl-index=0:
	private String opcion=null;
	private JButton jbtnRegistrar = null;
	private FrmMenu menu=null;

	public FrmRegistro(String opcion, FrmMenu menu) {
		super();
		this.opcion=opcion;
		this.menu=menu;
		initialize();
		bgTipoDocumento.add(jrdbTI);
		bgTipoDocumento.add(jrdbCC);
		bgInstitucion.add(jrdbInstitucion);
		bgInstitucion.add(jrdbOtra);
		bgTipoInstitucion.add(jrdbUniversidad);
		bgTipoInstitucion.add(jrdbColegio);
		if(opcion.equals("Estudiante")){
			jrdbInstitucion.setEnabled(true);
			jrdbOtra.setEnabled(true);
			jrdbTI.setEnabled(true);
			jrdbCC.setEnabled(true);
		}else if(opcion.equals("Docente")){
			jchkAlgebraLineal.setEnabled(true);
			jchkAlgebraVectorial.setEnabled(true);
			jchkGeometria.setEnabled(true);
			jchkEstadistica.setEnabled(true);
			jchkMatematicasOperativas.setEnabled(true);
			jchkCalculo.setEnabled(true);
			jchkFisica.setEnabled(true);
		}
	}

	private void initialize() {
		this.setBounds(0, 0, 603, 233);
		this.setLocationRelativeTo(menu);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setContentPane(getJContentPane());
		this.setTitle("Registro");
	}

	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jlbl = new JLabel();
			jlbl.setBounds(new Rectangle(320, 10, 45, 20));
			jlbl.setText("�reas:");
			jlblPrograma = new JLabel();
			jlblPrograma.setBounds(new Rectangle(10,150, 80, 20));
			jlblPrograma.setText("Programa");
			jlblInstitucion = new JLabel();
			jlblInstitucion.setBounds(new Rectangle(10, 100, 80, 20));
			jlblInstitucion.setText("Instituci�n");
			jlblDocumento = new JLabel();
			jlblDocumento.setBounds(new Rectangle(10, 10, 80, 20));
			jlblDocumento.setText("Documento");
			jlblApellido = new JLabel();
			jlblApellido.setBounds(new Rectangle(10, 70, 80, 20));
			jlblApellido.setText("Apellido");
			jlblNombre = new JLabel();
			jlblNombre.setBounds(new Rectangle(10, 40, 80, 20));
			jlblNombre.setText("Nombre");
			jContentPane = new JPanel();
			jContentPane.setLayout(null);
			jContentPane.add(jlblNombre, null);
			jContentPane.add(jlblApellido, null);
			jContentPane.add(jlblDocumento, null);
			jContentPane.add(getJtxtDocumento(), null);
			jContentPane.add(getJtxtNombre(), null);
			jContentPane.add(getJtxtApellido(), null);
			jContentPane.add(getJrdbTI(), null);
			jContentPane.add(getJrdbCC(), null);
			jContentPane.add(jlblInstitucion, null);
			jContentPane.add(getJrdbInstitucion(), null);
			jContentPane.add(getJrdbOtra(), null);
			jContentPane.add(getJtxtOtra(), null);
			jContentPane.add(getJrdbUniversidad(), null);
			jContentPane.add(getJrdbColegio(), null);
			jContentPane.add(jlblPrograma, null);
			jContentPane.add(getJtxtPrograma(), null);
			jContentPane.add(jlbl, null);
			jContentPane.add(getJchkMatematicasOperativas(), null);
			jContentPane.add(getJchkCalculo(), null);
			jContentPane.add(getJchkGeometria(), null);
			jContentPane.add(getJchkAlgebraLineal(), null);
			jContentPane.add(getJchkAlgebraVectorial(), null);
			jContentPane.add(getJchkFisica(), null);
			jContentPane.add(getJchkEstadistica(), null);
			jContentPane.add(getJbtnRegistrar(), null);
		}
		return jContentPane;
	}

	private JTextField getJtxtDocumento() {
		if (jtxtDocumento == null) {
			jtxtDocumento = new JTextField();
			jtxtDocumento.setBounds(new Rectangle(90, 10, 120, 20));
		}
		return jtxtDocumento;
	}

	private JTextField getJtxtNombre() {
		if (jtxtNombre == null) {
			jtxtNombre = new JTextField();
			jtxtNombre.setBounds(new Rectangle(90, 40, 120, 20));
		}
		return jtxtNombre;
	}

	private JTextField getJtxtApellido() {
		if (jtxtApellido == null) {
			jtxtApellido = new JTextField();
			jtxtApellido.setBounds(new Rectangle(90, 70, 120, 20));
		}
		return jtxtApellido;
	}

	private JRadioButton getJrdbTI() {
		if (jrdbTI == null) {
			jrdbTI = new JRadioButton();
			jrdbTI.setBounds(new Rectangle(210, 10, 45, 20));
			jrdbTI.setText("T.I");
			jrdbTI.setEnabled(false);
		}
		return jrdbTI;
	}

	private JRadioButton getJrdbCC() {
		if (jrdbCC == null) {
			jrdbCC = new JRadioButton();
			jrdbCC.setBounds(new Rectangle(255, 10, 45, 20));
			jrdbCC.setText("C.C");
			jrdbCC.setEnabled(false);
		}
		return jrdbCC;
	}

	private JRadioButton getJrdbInstitucion() {
		if (jrdbInstitucion == null) {
			jrdbInstitucion = new JRadioButton();
			jrdbInstitucion.setBounds(new Rectangle(90, 100, 230, 20));
			jrdbInstitucion.setText("Instituci�n Universitaria de Envigado");
			jrdbInstitucion.setEnabled(false);
			jrdbInstitucion.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					jtxtOtra.setEnabled(false);
					jrdbUniversidad.setEnabled(false);
					jrdbColegio.setEnabled(false);
					jtxtPrograma.setEnabled(true);
				}
			});
		}
		return jrdbInstitucion;
	}

	private JRadioButton getJrdbOtra() {
		if (jrdbOtra == null) {
			jrdbOtra = new JRadioButton();
			jrdbOtra.setBounds(new Rectangle(90, 120, 60, 20));
			jrdbOtra.setText("Otra:");
			jrdbOtra.setEnabled(false);
			jrdbOtra.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					jtxtOtra.setEnabled(true);
					jrdbUniversidad.setEnabled(true);
					jrdbColegio.setEnabled(true);
				}
			});
		}
		return jrdbOtra;
	}

	private JTextField getJtxtOtra() {
		if (jtxtOtra == null) {
			jtxtOtra = new JTextField();
			jtxtOtra.setBounds(new Rectangle(150, 120, 160, 20));
			jtxtOtra.setEnabled(false);
		}
		return jtxtOtra;
	}

	private JRadioButton getJrdbUniversidad() {
		if (jrdbUniversidad == null) {
			jrdbUniversidad = new JRadioButton();
			jrdbUniversidad.setBounds(new Rectangle(310, 120, 95, 20));
			jrdbUniversidad.setText("Universidad");
			jrdbUniversidad.setEnabled(false);
			jrdbUniversidad.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					jtxtPrograma.setEnabled(true);
				}
			});
		}
		return jrdbUniversidad;
	}

	private JRadioButton getJrdbColegio() {
		if (jrdbColegio == null) {
			jrdbColegio = new JRadioButton();
			jrdbColegio.setBounds(new Rectangle(405, 120, 95, 20));
			jrdbColegio.setText("Colegio");
			jrdbColegio.setEnabled(false);
			jrdbColegio.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					jtxtPrograma.setEnabled(false);
				}
			});
		}
		return jrdbColegio;
	}

	private JTextField getJtxtPrograma() {
		if (jtxtPrograma == null) {
			jtxtPrograma = new JTextField();
			jtxtPrograma.setBounds(new Rectangle(90, 150, 120, 20));
			jtxtPrograma.setEnabled(false);
		}
		return jtxtPrograma;
	}

	private JCheckBox getJchkMatematicasOperativas() {
		if (jchkMatematicasOperativas == null) {
			jchkMatematicasOperativas = new JCheckBox();
			jchkMatematicasOperativas.setBounds(new Rectangle(365, 70, 165, 20));
			jchkMatematicasOperativas.setText("Matem�ticas Operativas");
			jchkMatematicasOperativas.setEnabled(false);
		}
		return jchkMatematicasOperativas;
	}

	private JCheckBox getJchkCalculo() {
		if (jchkCalculo == null) {
			jchkCalculo = new JCheckBox();
			jchkCalculo.setBounds(new Rectangle(490, 30, 125, 20));
			jchkCalculo.setText("C�lculo");
			jchkCalculo.setEnabled(false);
		}
		return jchkCalculo;
	}

	private JCheckBox getJchkGeometria() {
		if (jchkGeometria == null) {
			jchkGeometria = new JCheckBox();
			jchkGeometria.setBounds(new Rectangle(365, 50, 125, 20));
			jchkGeometria.setText("Geometr�a");
			jchkGeometria.setEnabled(false);
		}
		return jchkGeometria;
	}

	private JCheckBox getJchkAlgebraLineal() {
		if (jchkAlgebraLineal == null) {
			jchkAlgebraLineal = new JCheckBox();
			jchkAlgebraLineal.setBounds(new Rectangle(365, 10, 125, 20));
			jchkAlgebraLineal.setText("�lgebra Lineal");
			jchkAlgebraLineal.setEnabled(false);
		}
		return jchkAlgebraLineal;
	}

	private JCheckBox getJchkAlgebraVectorial() {
		if (jchkAlgebraVectorial == null) {
			jchkAlgebraVectorial = new JCheckBox();
			jchkAlgebraVectorial.setBounds(new Rectangle(365, 30, 125, 20));
			jchkAlgebraVectorial.setText("�lgebra Vectorial");
			jchkAlgebraVectorial.setEnabled(false);
		}
		return jchkAlgebraVectorial;
	}

	private JCheckBox getJchkFisica() {
		if (jchkFisica == null) {
			jchkFisica = new JCheckBox();
			jchkFisica.setBounds(new Rectangle(490, 50, 125, 20));
			jchkFisica.setText("F�sica");
			jchkFisica.setEnabled(false);
		}
		return jchkFisica;
	}

	private JCheckBox getJchkEstadistica() {
		if (jchkEstadistica == null) {
			jchkEstadistica = new JCheckBox();
			jchkEstadistica.setBounds(new Rectangle(490, 10, 125, 20));
			jchkEstadistica.setText("Estad�stica");
			jchkEstadistica.setEnabled(false);
		}
		return jchkEstadistica;
	}

	private JButton getJbtnRegistrar() {
		if (jbtnRegistrar == null) {
			jbtnRegistrar = new JButton();
			jbtnRegistrar.setBounds(new Rectangle(460, 150, 110, 30));
			jbtnRegistrar.setText("Registrarse");
			jbtnRegistrar.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					guardarUsuario();
				}
			});
		}
		return jbtnRegistrar;
	}

	private int guardarUsuario(){
		if(validarDatos()==Const.RESULTADO_EXITOSO){
			String documento = jtxtDocumento.getText();
			String tipoDocumento=null;
			if(jrdbTI.isSelected())
				tipoDocumento="T.I";
			else if(jrdbCC.isSelected())
				tipoDocumento="C.C";
			String nombre = jtxtNombre.getText();
			String apellido = jtxtApellido.getText();

			if(opcion.equals("Estudiante")){
				String institucion=null;
				String tipoInstitucion=null;
				if(jrdbInstitucion.isSelected()){
					institucion="Institucion Universitaria de Envigado";
					tipoInstitucion="Universidad";
				}else if(jrdbOtra.isSelected()){
					institucion=jtxtOtra.getText();
					if(jrdbUniversidad.isSelected()){
						tipoInstitucion="Universidad";
					}else if(jrdbColegio.isSelected()){
						tipoInstitucion="Colegio";
					}
				}
				String programa=null;
				if(tipoInstitucion.equals("Universidad"))
					programa=jtxtPrograma.getText();
				Usuario estudiante = new Estudiante(documento, nombre, apellido, tipoDocumento, institucion, tipoInstitucion, programa);
				menu.gestorUsuario.guardarEstudiante((Estudiante)estudiante);
				JOptionPane.showMessageDialog(this, "Registro de estudiante exitoso");
				this.dispose();
				return Const.RESULTADO_EXITOSO;

			}else if(opcion.equals("Docente")){
				ArrayList<String> areas= new ArrayList<String>();
				if(jchkAlgebraLineal.isSelected())
					areas.add("Algebra Lineal");
				if(jchkAlgebraVectorial.isSelected())
					areas.add("Algebra Vectorial");
				if(jchkCalculo.isSelected())
					areas.add("Calculo");
				if(jchkEstadistica.isSelected())
					areas.add("Estadistica");
				if(jchkFisica.isSelected())
					areas.add("Fisica");
				if(jchkGeometria.isSelected())
					areas.add("Geometria");
				if(jchkMatematicasOperativas.isSelected())
					areas.add("Matematicas Operativas");
				Usuario docente = new Docente(documento, nombre, apellido, areas);
				menu.gestorUsuario.guardarDocente((Docente)docente);
				JOptionPane.showMessageDialog(this, "Registro de docente exitoso");
				this.dispose();
				return Const.RESULTADO_EXITOSO;
			}
		}
		return Const.RESULTADO_FALLIDO;
	}

	private int validarDatos(){
		if(jtxtDocumento.getText().length()==0 || jtxtNombre.getText().length()==0 || jtxtApellido.getText().length()==0){
			JOptionPane.showMessageDialog(this, "Todos los campos habilitados son obligatorios");
			return Const.RESULTADO_FALLIDO;
		}
		if(opcion.equals("Estudiante")){
			if(menu.gestorUsuario.buscarEstudiante(jtxtDocumento.getText())!=null){
				JOptionPane.showMessageDialog(this, "El documento ingresado ya existe");
				return Const.RESULTADO_FALLIDO;
			}	
			if(!jrdbTI.isSelected() && !jrdbCC.isSelected()){
				JOptionPane.showMessageDialog(this, "Seleccione si su documento es T.I o C.C");
				return Const.RESULTADO_FALLIDO;
			}
			if(!jrdbInstitucion.isSelected() && !jrdbOtra.isSelected()){
				JOptionPane.showMessageDialog(this, "Debe registrar la instituci�n a la cual pertenece");
				return Const.RESULTADO_FALLIDO;
			}
			if(jrdbOtra.isSelected()){
				if(jtxtOtra.getText().length()==0){
					JOptionPane.showMessageDialog(this, "Todos los campos habilitados son obligatorios");
					return Const.RESULTADO_FALLIDO;
				}
				if(!jrdbUniversidad.isSelected() && !jrdbColegio.isSelected()){
					JOptionPane.showMessageDialog(this, "Seleccione si su instituci�n es Universidad o Colegio");
					return Const.RESULTADO_FALLIDO;
				}	
			}
			if((jrdbInstitucion.isSelected() || jrdbUniversidad.isSelected()) && jtxtPrograma.getText().length()==0){
				JOptionPane.showMessageDialog(this, "Todos los campos habilitados son obligatorios");
				return Const.RESULTADO_FALLIDO;
			}

		}else if(opcion.equals("Docente")){
			if(menu.gestorUsuario.buscarDocente(jtxtDocumento.getText())!=null){
				JOptionPane.showMessageDialog(this, "El documento ingresado ya existe");
				return Const.RESULTADO_FALLIDO;
			}
			if(!jchkAlgebraLineal.isSelected() && !jchkAlgebraVectorial.isSelected() && !jchkGeometria.isSelected() && !jchkMatematicasOperativas.isSelected() && !jchkEstadistica.isSelected() && !jchkCalculo.isSelected() && !jchkFisica.isSelected()){
				JOptionPane.showMessageDialog(this, "Seleccione las �reas que asesor�");
				return Const.RESULTADO_FALLIDO;
			}
		}
		return Const.RESULTADO_EXITOSO;
	}
}  //  @jve:decl-index=0:visual-constraint="10,10"

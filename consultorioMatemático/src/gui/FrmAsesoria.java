package gui;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JLabel;

import java.awt.Rectangle;

import javax.swing.JTextField;
import javax.swing.JComboBox;

import java.util.ArrayList;

import javax.swing.JButton;

import funcionalidad.datos.Asesoria;
import funcionalidad.datos.Docente;
import funcionalidad.datos.Estudiante;
import util.Const;

public class FrmAsesoria extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel jContentPane = null;
	private JLabel jlblDocumento = null;
	private JTextField jtxtDocumento = null;
	private JLabel jlblDocente = null;
	private JComboBox<String> jcbxDocente = null;
	private JLabel jlblTematica = null;
	private JTextField jtxtTematica = null;
	private JLabel jlblFecha = null;
	private JLabel jlblDia = null;
	private JLabel jlblMes = null;
	private JLabel jlblA�o = null;
	private JTextField jtxtDia = null;
	private JTextField jtxtMes = null;
	private JTextField jtxtA�o = null;
	private JButton jbtnGuardar = null;
	private FrmMenu menu=null;
	
	public FrmAsesoria(FrmMenu menu) {
		super();
		this.menu=menu;
		initialize();
	}

	private void initialize() {
		this.setSize(338, 213);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setContentPane(getJContentPane());
		this.setTitle("Asesor�a");
		
	}

	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jlblA�o = new JLabel();
			jlblA�o.setBounds(new Rectangle(230, 100, 30, 20));
			jlblA�o.setText("A�o");
			jlblMes = new JLabel();
			jlblMes.setBounds(new Rectangle(150, 100, 30, 20));
			jlblMes.setText("Mes");
			jlblDia = new JLabel();
			jlblDia.setBounds(new Rectangle(70, 100, 30, 20));
			jlblDia.setText("D�a");
			jlblFecha = new JLabel();
			jlblFecha.setBounds(new Rectangle(10, 100, 40, 20));
			jlblFecha.setText("Fecha:");
			jlblTematica = new JLabel();
			jlblTematica.setBounds(new Rectangle(10, 70, 150, 20));
			jlblTematica.setText("Tem�tica");
			jlblDocente = new JLabel();
			jlblDocente.setBounds(new Rectangle(10, 40, 150, 20));
			jlblDocente.setText("Nombre del docente");
			jlblDocumento = new JLabel();
			jlblDocumento.setBounds(new Rectangle(10, 10, 150, 20));
			jlblDocumento.setText("Documento del estudiante");
			jContentPane = new JPanel();
			jContentPane.setLayout(null);
			jContentPane.add(jlblDocumento, null);
			jContentPane.add(getJtxtDocumento(), null);
			jContentPane.add(jlblDocente, null);
			jContentPane.add(getJcbxDocente(), null);
			jContentPane.add(jlblTematica, null);
			jContentPane.add(getJtxtTematica(), null);
			jContentPane.add(jlblFecha, null);
			jContentPane.add(jlblDia, null);
			jContentPane.add(jlblMes, null);
			jContentPane.add(jlblA�o, null);
			jContentPane.add(getJtxtDia(), null);
			jContentPane.add(getJtxtMes(), null);
			jContentPane.add(getJtxtA�o(), null);
			jContentPane.add(getJbtnGuardar(), null);
		}
		return jContentPane;
	}

	private JTextField getJtxtDocumento() {
		if (jtxtDocumento == null) {
			jtxtDocumento = new JTextField();
			jtxtDocumento.setBounds(new Rectangle(160, 10, 150, 20));
		}
		return jtxtDocumento;
	}

	private JComboBox<String> getJcbxDocente() {
		if (jcbxDocente == null) {
			jcbxDocente = new JComboBox<String>();
			jcbxDocente.addItem("Seleccionar");
			ArrayList<Docente> aux = menu.gestorUsuario.getDocentes();
			for(int i=0;i<aux.size();i++){
				jcbxDocente.addItem(aux.get(i).getNombre());
			}
			jcbxDocente.setBounds(new Rectangle(160, 40, 150, 20));
		}
		return jcbxDocente;
	}

	private JTextField getJtxtTematica() {
		if (jtxtTematica == null) {
			jtxtTematica = new JTextField();
			jtxtTematica.setBounds(new Rectangle(160, 70, 150, 20));
		}
		return jtxtTematica;
	}

	private JTextField getJtxtDia() {
		if (jtxtDia == null) {
			jtxtDia = new JTextField();
			jtxtDia.setBounds(new Rectangle(100, 100, 40, 20));
		}
		return jtxtDia;
	}

	private JTextField getJtxtMes() {
		if (jtxtMes == null) {
			jtxtMes = new JTextField();
			jtxtMes.setBounds(new Rectangle(180, 100, 40, 20));
		}
		return jtxtMes;
	}

	private JTextField getJtxtA�o() {
		if (jtxtA�o == null) {
			jtxtA�o = new JTextField();
			jtxtA�o.setBounds(new Rectangle(260, 100, 40, 20));
		}
		return jtxtA�o;
	}

	private JButton getJbtnGuardar() {
		if (jbtnGuardar == null) {
			jbtnGuardar = new JButton();
			jbtnGuardar.setBounds(new Rectangle(115, 135, 93, 30));
			jbtnGuardar.setText("Guardar");
			jbtnGuardar.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					guardarAsesoria();
				}
			});
		}
		return jbtnGuardar;
	}
	
	private int guardarAsesoria(){
		if(validarDatos()==Const.RESULTADO_EXITOSO){
			Estudiante estudiante = menu.gestorUsuario.buscarEstudiante(jtxtDocumento.getText());
			Docente docente = menu.gestorUsuario.buscarNombreDocente((String)jcbxDocente.getSelectedItem());
			
			String tematica = jtxtTematica.getText();
			String fecha = jtxtDia.getText()+"/"+jtxtMes.getText()+"/"+jtxtA�o.getText();
			Asesoria asesoria = new Asesoria(estudiante, docente, fecha, tematica);
			
			
			menu.gestorAsesoria.guardarAsesoria(asesoria);
			
			JOptionPane.showMessageDialog(this, "Asesor�a guardada exitosamente");
			this.dispose();
			return Const.RESULTADO_EXITOSO;
		}
		return Const.RESULTADO_FALLIDO;
	}
	
	private int validarDatos(){
		if(jtxtDocumento.getText().length()==0 || jtxtTematica.getText().length()==0 || jtxtDia.getText().length()==0 || jtxtMes.getText().length()==0 || jtxtA�o.getText().length()==0){
			JOptionPane.showMessageDialog(this, "Todos los campos habilitados son obligatorios");
			return Const.RESULTADO_FALLIDO;
		}
		if(menu.gestorUsuario.buscarEstudiante(jtxtDocumento.getText())==null){
			JOptionPane.showMessageDialog(this, "El documento ingresado no existe");
			return Const.RESULTADO_FALLIDO;
		}
		if(jcbxDocente.getSelectedIndex()==0){
			JOptionPane.showMessageDialog(this, "Seleccione el docente que lo asesorar�");
			return Const.RESULTADO_FALLIDO;
		}
		return Const.RESULTADO_EXITOSO;
	}
}

package gui;

import java.util.ArrayList;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JFrame;

import funcionalidad.datos.Estudiante;
import funcionalidad.operaciones.GestorAsesoria;
import funcionalidad.operaciones.GestorUsuario;

public class FrmMenu extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel jContentPane = null;
	private JMenuBar jmb = null;
	private JMenu jmRegistro=null;
	private JMenu jmAsesoria=null;
	private JMenu jmConsulta=null;
	private JMenuItem jmiRegistro=null;
	private JMenuItem jmiAsesoria=null;
	private JMenuItem jmiTotalAsesorados=null;
	private JMenuItem jmiAsesoradosPeriodo=null;
	private JMenuItem jmiAsesoradosDocente=null;
	private JMenuItem jmiEstudiantesAsesoradosDocente=null;
	public GestorUsuario gestorUsuario=null;
	public GestorAsesoria gestorAsesoria=null;
	
	public FrmMenu(GestorUsuario gestorUsuario) {
		super();
		this.gestorUsuario=gestorUsuario;
		this.gestorAsesoria=GestorAsesoria.getInstance();
		initialize();
	}

	private void initialize() {
		this.setSize(700, 424);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setContentPane(getJContentPane());
		this.setTitle("Consultorio Matem�tico");
		this.setJMenuBar(jmb);
	}

	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(null);
			jmb = new JMenuBar();
			jmb.setSize(getMaximumSize());
			
			//Menu1 Item1
			jmRegistro=new JMenu("Registro");
			jmiRegistro=new JMenuItem("Registrarse...");
			jmiRegistro.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					mostrarRegistro();
				}
			});
			jmRegistro.add(jmiRegistro);
			
			//Menu2 Item1
			jmAsesoria=new JMenu("Asesor�a");
			jmiAsesoria=new JMenuItem("Tomar asesor�a...");
			jmiAsesoria.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					mostrarAsesoria();
				}
			});
			jmAsesoria.add(jmiAsesoria);
			
			//Menu3 Item1
			jmConsulta=new JMenu("Consulta");
			jmiTotalAsesorados=new JMenuItem("Cantidad total de asesorados...");
			jmiTotalAsesorados.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					mostrarCantidadTotalAsesorados();
				}
			});
			jmConsulta.add(jmiTotalAsesorados);
			
			//Menu3 Item2
			jmiAsesoradosPeriodo=new JMenuItem("Cantidad de asesorados por periodo...");
			jmiAsesoradosPeriodo.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					mostrarConsultaPeriodo();
				}
			});
			jmConsulta.add(jmiAsesoradosPeriodo);
			
			//Menu3 Item3
			jmiAsesoradosDocente=new JMenuItem("Cantidad de asesorados por docente...");
			jmiAsesoradosDocente.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					mostrarCantidadAsesoradosDocente();
				}
			});
			jmConsulta.add(jmiAsesoradosDocente);
			
			//Menu3 Item4
			jmiEstudiantesAsesoradosDocente=new JMenuItem("Estudiantes asesorados por docente...");
			jmiEstudiantesAsesoradosDocente.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					mostrarAsesoradosDocente();
				}
			});
			jmConsulta.add(jmiEstudiantesAsesoradosDocente);
			
			jmb.add(jmRegistro);
			jmb.add(jmAsesoria);
			jmb.add(jmConsulta);
		}
		return jContentPane;
	}

	private void mostrarAsesoria(){
		if(gestorUsuario.getDocentes().size()>0)
			new FrmAsesoria(this).setVisible(true);
		else
			JOptionPane.showMessageDialog(this, "No hay docentes registrados");
	}

	private void mostrarRegistro(){
		String[] v={"Estudiante", "Docente"};
		String opcion=(String)JOptionPane.showInputDialog(this, "Seleccione una opci�n de registro", null, JOptionPane.QUESTION_MESSAGE,  null, v, v[0]);
		if(opcion==null)
			return;
		new FrmRegistro(opcion, this).setVisible(true);
	}
	
	private void mostrarCantidadTotalAsesorados(){
		JOptionPane.showMessageDialog(this, "Cantidad total de asesorados: "+gestorAsesoria.cantidadAsesoradosTotal());
	}
	
	private void mostrarConsultaPeriodo(){
		new FrmConsultaPeriodo(this).setVisible(true);
	}
	
	private void mostrarCantidadAsesoradosDocente(){
		if(gestorUsuario.getDocentes().size()==0){
			JOptionPane.showMessageDialog(this, "No hay docentes registrados");
			return;
		}
		String[] v=new String[gestorUsuario.getDocentes().size()];
		for(int i=0; i<v.length; i++){
			v[i]=gestorUsuario.getDocentes().get(i).getNombre();
		}
		String nombreDocente=(String)JOptionPane.showInputDialog(this, "Seleccione un docente", null, JOptionPane.QUESTION_MESSAGE,  null, v, v[0]);
		int cantidad = gestorAsesoria.cantidadAsesoradosPorDocente(nombreDocente);
		JOptionPane.showMessageDialog(this, nombreDocente+" "+"ha asesorado "+cantidad+" personas");
	}
	
	private void mostrarAsesoradosDocente(){
		if(gestorUsuario.getDocentes().size()==0){
			JOptionPane.showMessageDialog(this, "No hay docentes registrados");
			return;
		}
		String[] v=new String[gestorUsuario.getDocentes().size()];
		for(int i=0; i<v.length; i++){
			v[i]=gestorUsuario.getDocentes().get(i).getNombre();
		}
		String nombreDocente=(String)JOptionPane.showInputDialog(this, "Seleccione un docente", null, JOptionPane.QUESTION_MESSAGE,  null, v, v[0]);
		ArrayList<Estudiante> asesorados = gestorAsesoria.asesoradosPorDocente(nombreDocente);
		String mensaje="";
		for(int i=0; i<asesorados.size(); i++){
			mensaje=mensaje+"\n"+asesorados.get(i).getNombre();
		}
		if(mensaje.length()==0){
			JOptionPane.showMessageDialog(this, nombreDocente+" no ha dado asesorias");
			return;
		}
		JOptionPane.showMessageDialog(this, nombreDocente+" ha asesorado a los siguientes estudiantes:"+mensaje);
	}
}  //  @jve:decl-index=0:visual-constraint="10,10"

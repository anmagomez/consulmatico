package gui;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JLabel;

import java.awt.Rectangle;

import javax.swing.JTextField;
import javax.swing.JButton;

import util.Const;

public class FrmConsultaPeriodo extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel jContentPane = null;
	private JLabel jlblFInicial = null;
	private JLabel jlblFFinal = null;
	private JLabel jlblDiaI = null;
	private JLabel jlblDiaF = null;
	private JLabel jlblMesI = null;
	private JLabel jlblMesF = null;
	private JLabel jlblA�oI = null;
	private JLabel jlblA�oF = null;
	private JTextField jtxtDiaI = null;
	private JTextField jtxtMesI = null;
	private JTextField jtxtA�oI = null;
	private JTextField jtxtDiaF = null;
	private JTextField jtxtMesF = null;
	private JTextField jtxtA�oF = null;
	private JButton jbtnConsulta = null;
	private FrmMenu menu = null;

	public FrmConsultaPeriodo(FrmMenu menu) {
		super();
		this.menu=menu;
		initialize();
	}

	private void initialize() {
		this.setSize(355, 154);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setContentPane(getJContentPane());
		this.setTitle("Consulta ");
	}

	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jlblA�oF = new JLabel();
			jlblA�oF.setBounds(new Rectangle(265, 40, 25, 20));
			jlblA�oF.setText("A�o");
			jlblA�oI = new JLabel();
			jlblA�oI.setBounds(new Rectangle(95, 40, 25, 20));
			jlblA�oI.setText("A�o");
			jlblMesF = new JLabel();
			jlblMesF.setBounds(new Rectangle(220, 40, 25, 20));
			jlblMesF.setText("Mes");
			jlblMesI = new JLabel();
			jlblMesI.setBounds(new Rectangle(50, 40, 25, 20));
			jlblMesI.setText("Mes");
			jlblDiaF = new JLabel();
			jlblDiaF.setBounds(new Rectangle(180, 40, 20, 20));
			jlblDiaF.setText("D�a");
			jlblDiaI = new JLabel();
			jlblDiaI.setBounds(new Rectangle(10, 40, 20, 20));
			jlblDiaI.setText("D�a");
			jlblFFinal = new JLabel();
			jlblFFinal.setBounds(new Rectangle(220, 10, 70, 20));
			jlblFFinal.setText("Fecha Final");
			jlblFInicial = new JLabel();
			jlblFInicial.setBounds(new Rectangle(50, 10, 70, 20));
			jlblFInicial.setText("Fecha Inicial");
			jContentPane = new JPanel();
			jContentPane.setLayout(null);
			jContentPane.add(jlblFInicial, null);
			jContentPane.add(jlblFFinal, null);
			jContentPane.add(jlblDiaI, null);
			jContentPane.add(jlblDiaF, null);
			jContentPane.add(jlblMesI, null);
			jContentPane.add(jlblMesF, null);
			jContentPane.add(jlblA�oI, null);
			jContentPane.add(jlblA�oF, null);
			jContentPane.add(getJtxtDiaI(), null);
			jContentPane.add(getJtxtMesI(), null);
			jContentPane.add(getJtxtA�oI(), null);
			jContentPane.add(getJtxtDiaF(), null);
			jContentPane.add(getJtxtMesF(), null);
			jContentPane.add(getJtxtA�oF(), null);
			jContentPane.add(getJbtnConsulta(), null);
		}
		return jContentPane;
	}

	private JTextField getJtxtDiaI() {
		if (jtxtDiaI == null) {
			jtxtDiaI = new JTextField();
			jtxtDiaI.setBounds(new Rectangle(30, 40, 20, 20));
		}
		return jtxtDiaI;
	}

	private JTextField getJtxtMesI() {
		if (jtxtMesI == null) {
			jtxtMesI = new JTextField();
			jtxtMesI.setBounds(new Rectangle(75, 40, 20, 20));
		}
		return jtxtMesI;
	}

	private JTextField getJtxtA�oI() {
		if (jtxtA�oI == null) {
			jtxtA�oI = new JTextField();
			jtxtA�oI.setBounds(new Rectangle(120, 40, 40, 20));
		}
		return jtxtA�oI;
	}

	private JTextField getJtxtDiaF() {
		if (jtxtDiaF == null) {
			jtxtDiaF = new JTextField();
			jtxtDiaF.setBounds(new Rectangle(200, 40, 20, 20));
		}
		return jtxtDiaF;
	}

	private JTextField getJtxtMesF() {
		if (jtxtMesF == null) {
			jtxtMesF = new JTextField();
			jtxtMesF.setBounds(new Rectangle(245, 40, 20, 20));
		}
		return jtxtMesF;
	}

	private JTextField getJtxtA�oF() {
		if (jtxtA�oF == null) {
			jtxtA�oF = new JTextField();
			jtxtA�oF.setBounds(new Rectangle(290, 40, 40, 20));
		}
		return jtxtA�oF;
	}

	private JButton getJbtnConsulta() {
		if (jbtnConsulta == null) {
			jbtnConsulta = new JButton();
			jbtnConsulta.setBounds(new Rectangle(100, 75, 140, 30));
			jbtnConsulta.setText("Realizar Consulta");
			jbtnConsulta.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					realizarConsulta();
				}
			});
		}
		return jbtnConsulta;
	}
	
	private void realizarConsulta(){
		if(validarDatos()==Const.RESULTADO_EXITOSO){
			String fechaInicial=jtxtDiaI.getText()+"/"+jtxtMesI.getText()+"/"+jtxtA�oI.getText();
			String fechaFinal=jtxtDiaF.getText()+"/"+jtxtMesF.getText()+"/"+jtxtA�oF.getText();
			JOptionPane.showMessageDialog(this, "Cantidad de asesorados en el periodo seleccionado: "+menu.gestorAsesoria.asesoradosPorPeriodo(fechaInicial, fechaFinal));
			this.dispose();
		}
	}
	
	private int validarDatos(){
		if(jtxtDiaI.getText().length()==0||jtxtMesI.getText().length()==0||jtxtA�oI.getText().length()==0||jtxtDiaF.getText().length()==0||jtxtMesF.getText().length()==0||jtxtA�oF.getText().length()==0){
			JOptionPane.showMessageDialog(this, "Todos los campos habilitados son obligatorios");
			return Const.RESULTADO_FALLIDO;
		}
		return Const.RESULTADO_EXITOSO;
	}

}  //  @jve:decl-index=0:visual-constraint="10,10"

package principal;

import funcionalidad.operaciones.GestorAsesoria;
import funcionalidad.operaciones.GestorUsuario;
import gui.FrmMenu;
/**
 * 
 * @author Sebastian Zapata. Estudiante de Ingenier�a de Sistemas. IUE. 2014
 * Este programa responde al siguiente enunciado. Es un trabajo de clase, por tanto es posible 
 * que existan algunos aspectos que no se implementaron completamente. 
 * Se hicieron unas modificaciones de lo presentado por el autor con su debida autorizaci�n para 
 * que el c�gido fuese usado como herramienta pedag�gica.
 * 
 * Nombre de la aplicaci�n: Atenci�n de estudiantes en el consultorio Matem�tico de la IUE
 * En el momento la IUE cuenta con un consultorio matem�tico que atiende estudiantes tanto de la instituci�n como de otras instituciones. 
 * El proceso de registro de la atenci�n se realiza en forma manual, lo que dificulta la generaci�n de estad�sticas para la gesti�n del consultorio 
 * as� como la actualizaci�n de los sistemas de informaci�n con los que cuenta la instituci�n. Por ello se requiere una aplicaci�n que permita realizar
 * el registro de las personas que reciben asesor�a en el consultorio. La aplicaci�n tendr� tres m�dulos que se describen a continuaci�n, 
 * sin embargo usted elaborar� solo lo que se pide en la tabla d�nde se le indica que deber� realizar esta aplicaci�n. 
 * 
 * M�dulo de ingreso de estudiantes: 
 * Consiste en el registro de los datos de las personas que hacen uso del consultorio. 
 * Cuando un estudiante hace uso por primera vez del consultorio debe diligenciar un formulario de registro. 
 * En el formulario el estudiante ingresa, su documento de identidad (que puede ser c�dula o tarjeta de identidad), 
 * sus nombres y apellidos, el programa acad�mico en el que se encuentra matriculado si es estudiante de la instituci�n 
 * o la instituci�n de la que proviene si es externo. Si es externo tambi�n debe indicar si se encuentra estudiando en colegio
 * o en universidad e indicar el nombre de la instituci�n en la que estudia. El estudiante ingresa sus datos de registro solo una vez. 
 * Se debe garantizar que la identificaci�n del estudiante sea �nica en el sistema.
 * Si es primera vez, luego de haberse registrado, el estudiante debe seleccionar con qu� profesor recibir� la asesor�a
 * (esto de los profesores que se encuentran en la sala en ese momento) y digitar� la tem�tica sobre la que desea consultar 
 * y si el estudiante es de la IUE, deber� seleccionar adem�s la asignatura por la que est� consultando. 
 * Si es no es la primera vez que el estudiante acude al consultorio, solo deber� ingresar su n�mero de identificaci�n, 
 * seleccionar el profesor e ingresar la tem�tica.
 * Cuando se hace el ingreso del estudiante en el sistema sus datos se almacenan en forma permanente. Todos los campos a diligenciar en el formulario
 * son obligatorios. La aplicaci�n deber� registrar la fecha en la que el estudiante acudi� a la asesor�a.
 * 
 * M�dulo de registro de docentes:
 * En este m�dulo, los docentes se registran con su n�mero de identificaci�n, su nombre y el �rea o �reas que asesora
 * y que pueden ser: Matem�ticas Operativas, C�lculo, Geometr�a �lgebra Lineal y Vectorial, F�sica y Estad�stica. Un docente puede asesorar varias �reas.
 * (Opcional: cuando el docente llega al consultorio se registra, de manera que cuando el estudiante llegue encuentre el listado de docentes que hay en
 * ese momento en el consultorio)
 * 
 * M�dulo de consultas:
 * En este m�dulo se podr�n realizar y mostrar los resultados de las consultas que se hacen sobre los datos que han almacenado los estudiantes.
 * Las opciones son las siguientes:
 * Asesor�as por docente: Se mostrar�n los estudiantes que ha asistido a las asesor�as de un docente particular. 
 * El formato en el que se presenta esta informaci�n debe ser el proporcionado por el docente en este enunciado.
 * N�mero de estudiantes que asisten en un periodo. Se podr� consultar la cantidad de estudiantes que asisten entre una 
 * fecha inicial y una fecha final.
 * N�mero de estudiantes que un docente ha atendido y n�mero total de estudiantes atendidos.

 *
 */
public class Main {
	public static void main(String[] args) {
		new FrmMenu(new GestorUsuario()).setVisible(true);
	}
}

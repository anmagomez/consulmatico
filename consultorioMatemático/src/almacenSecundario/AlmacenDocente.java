package almacenSecundario;

import java.util.ArrayList;

import util.Const;
import funcionalidad.datos.Docente;

public class AlmacenDocente {
	private ArchivoTexto archivoDocente;
	
	public AlmacenDocente(){
		archivoDocente=new ArchivoTexto("archivos/docentes.txt");
	}
	public Docente consultarDocente(String codigo){
		ArrayList<String> listaDocentes=archivoDocente.listarArchivo();

		for(String registroDocente: listaDocentes){
			String[] datos=registroDocente.split(";");
			if(datos[0].equals(codigo)){

				String nombre=datos[1];
				String apellido=datos[2];
				String[] areas=datos[3].split("~");
				ArrayList<String> datosAreas=new ArrayList<String>();

				for(int i=0;i<areas.length;i++){
					datosAreas.add(areas[i]);
				}
				Docente objDocente=new Docente(codigo,nombre,apellido,datosAreas);
				return objDocente;
			}
		}
		return null;

	}
	public ArrayList<Docente> listarDocentes(){
		ArrayList<String> listaDocentes=archivoDocente.listarArchivo();
		if(listaDocentes==null)
			return null;
		ArrayList<Docente> listado=new ArrayList<Docente>();

		for(String registroDocente: listaDocentes){
			String[] datos=registroDocente.split(";");
			String codigo=datos[0];
			String nombre=datos[1];
			String apellido=datos[2];
			String[] areas=datos[3].split("~");
			ArrayList<String> datosAreas=new ArrayList<String>();

			for(int i=0;i<areas.length;i++){
				datosAreas.add(areas[i]);
			}
			Docente objDocente=new Docente(codigo,nombre,apellido,datosAreas);
			listado.add(objDocente);
		}
		return listado;
	}

	public int guardarDocente(Docente docente){
		if(archivoDocente==null)
			return Const.RESULTADO_FALLIDO;

		String registroDocente=docente.getDocumento()+";"
				+docente.getNombre()+";"
				+docente.getApellido()+";";

		ArrayList<String> areas=docente.getAreas();
		for(String area:areas){
			registroDocente=registroDocente+area+"~";
		}

		archivoDocente.escribirAlFinal(registroDocente);
		return Const.RESULTADO_EXITOSO;
	}

}

package almacenSecundario;


import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class ArchivoTexto {

	private File objFile;
	private String ruta;

	public ArchivoTexto(File objFile) {
		this.objFile = objFile;
		this.ruta = objFile.getAbsolutePath();
	}

	public ArchivoTexto(String ruta) {
		this(true, ruta);
	}

	public ArchivoTexto(boolean crear, String ruta) {
		if (crear) {
			objFile = new File(ruta); //CREA LA INSTANCIA

			try {
				objFile.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public boolean existe() {
		return objFile.exists();
	}

	public boolean esArchivo() {
		return objFile.isFile();
	}

	public boolean esDirectorio() {
		return objFile.isDirectory();
	}

	public boolean sePuedeLeer() {
		return objFile.canRead();
	}

	public boolean sePuedeEscribir() {
		return objFile.canWrite();
	}

	public String getNombre() {
		return objFile.getName();
	}

	public long getLongitud() {
		return objFile.length();
	}

	public String getRuta() {
		return objFile.getAbsolutePath();
	}

	public String leerArchivo() {
		String salida = "";
		FileReader fr;
		try {
			fr = new FileReader(objFile);
		} catch (FileNotFoundException e1) {

			e1.printStackTrace();
			return "Error: Archivo no encontrado";
		} //Crear el objeto para el archivo de lectura
		try {
			//La clase BufferedReader permite preparar el espacio para el almacenamiento de los datos que se leen desde el archivo

			BufferedReader entrada = new BufferedReader(fr);
			String linea;
			//El ciclo, lee l�nea a l�nea lo que se almacen� en el archivo
			while ((linea = entrada.readLine()) != null) {
				salida = salida + "\n" + linea;
			}

		} catch (IOException ioe) {
			// TODO Auto-generated catch block

			ioe.printStackTrace();
			return "Error al procesar el archivo o archivo no encontrado";
		} finally {
			try {
				if (fr != null) // Un archivo siempre debe cerrarse despu�s de usarlo
				{
					fr.close();
				}

			} catch (Exception e) {
				e.printStackTrace();
				return "Error al cerrar el archivo o archivo no encontrado";

			}
		}
		return salida;
	}

	public ArrayList<String> listarArchivo() {
		ArrayList<String> listaSalida = null;
		FileReader fr = null;

		try {
			fr = new FileReader(objFile);

			BufferedReader br = new BufferedReader(fr);
			//Cada l�nea ser� guardada en la lista
			String linea;
			listaSalida = new ArrayList<String>();
			while ((linea = br.readLine()) != null) {
				listaSalida.add(linea);
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fr != null) {
					fr.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return listaSalida;

	}

	public void sobreEscribir(String contenido) {
		FileWriter fichero = null;
		PrintWriter pw = null;
		try {
			fichero = new FileWriter(objFile);
			pw = new PrintWriter(fichero);

			pw.print(contenido);
			pw.println();//Hace un salto de l�nea
			//flush escribe directamente en el archivo 
			fichero.flush();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// Este bloque se ejecuta en cualquier caso
			try {

				if (null != fichero) {
					fichero.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void escribirAlFinal(String contenido) {
		FileWriter fichero = null;
		PrintWriter pw = null;
		try {
			fichero = new FileWriter(objFile, true); // el segundo par�metro indica si lo que se va a escribir en el archivo, se adiciona al final o no
			pw = new PrintWriter(fichero);


			pw.print(contenido);
			pw.println();//Hace un salto de l�nea

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// Este bloque se ejecuta en cualquier caso
			try {

				if (null != fichero) {
					fichero.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}

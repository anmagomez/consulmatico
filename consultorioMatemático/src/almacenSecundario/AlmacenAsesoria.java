package almacenSecundario;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;



import util.Const;
import funcionalidad.datos.Asesoria;
import funcionalidad.datos.Docente;
import funcionalidad.datos.Estudiante;

public class AlmacenAsesoria {
	
	private ArchivoTexto archivoAsesorias;
	
	public AlmacenAsesoria(){ 
		archivoAsesorias=new ArchivoTexto("archivos/fileAsesoria.txt");
	}

	public int guardarAsesoria(Asesoria asesoria) {
		if(archivoAsesorias==null)
			return Const.RESULTADO_FALLIDO;
		String registroAsesoria=asesoria.getFecha()+";"
				+asesoria.getDocente().getDocumento()+";"
				+asesoria.getEstudiante().getDocumento()+";"
				+asesoria.getTematica();
		archivoAsesorias.escribirAlFinal(registroAsesoria);
		return Const.RESULTADO_EXITOSO;
	}

	public ArrayList<Asesoria> listarAsesorias(){
		if(archivoAsesorias==null)
			return null;
		ArrayList<String> asesoriasArchivo=archivoAsesorias.listarArchivo();
		ArrayList<Asesoria> listaAsesorias=new ArrayList<Asesoria>();
		
		for(String registro:asesoriasArchivo){
			String datos[]=registro.split(";");
			
			
			String fecha=datos[0];
			String codDocente=datos[1];
			String codEstudiante=datos[2];
			String tematica=datos[3];
			
			AlmacenEstudiante almEstudiante=new AlmacenEstudiante();
			Estudiante estudiante=almEstudiante.consultarEstudiante(codEstudiante);
			AlmacenDocente almDocente=new AlmacenDocente();
			Docente docente=almDocente.consultarDocente(codDocente);
			
			Asesoria newAsesoria=new Asesoria(estudiante,docente,fecha,tematica);
			listaAsesorias.add(newAsesoria);
		}
		return listaAsesorias;
	}



}

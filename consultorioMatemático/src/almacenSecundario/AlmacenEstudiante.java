package almacenSecundario;

import java.util.ArrayList;

import util.Const;
import funcionalidad.datos.Estudiante;

public class AlmacenEstudiante {
	private ArchivoTexto archivoEstudiante;

	public AlmacenEstudiante(){
		archivoEstudiante=new ArchivoTexto("archivos/estudiantes.txt");
	}
	public Estudiante consultarEstudiante(String codigo){

		ArrayList<String> listaEstudiantes=archivoEstudiante.listarArchivo();

		for(String registroEstudiante: listaEstudiantes){
			String[] datos=registroEstudiante.split(";");
			if(datos[0].equals(codigo)){
				String tipoDocumento=datos[1];
				String nombre=datos[2];
				String apellido=datos[3];
				String programa=datos[4];
				String institucion=datos[5];
				String tipoInstitucion=datos[6];

				Estudiante objEstudiante=new Estudiante(codigo,nombre,apellido,tipoDocumento,institucion,tipoInstitucion,programa);
				return objEstudiante;
			}
		}
		return null;

	}
	public ArrayList<Estudiante> listarEstudiantes(){
		ArrayList<String> listaEstudiantes=archivoEstudiante.listarArchivo();
		ArrayList<Estudiante> listado=new ArrayList<Estudiante>();
		for(String registroEstudiante: listaEstudiantes){
			String[] datos=registroEstudiante.split(";");

			String codigo=datos[0];
			String tipoDocumento=datos[1];
			String nombre=datos[2];
			String apellido=datos[3];
			String programa=datos[4];
			String institucion=datos[5];
			String tipoInstitucion=datos[6];

			Estudiante objEstudiante=new Estudiante(codigo,nombre,apellido,tipoDocumento,institucion,tipoInstitucion,programa);
			listado.add(objEstudiante);
		}

		return listado;
	}
	public int guardarEstudiante(Estudiante estudiante){
		if(archivoEstudiante==null)
			return Const.RESULTADO_FALLIDO;

		String registroEstudiante=estudiante.getDocumento()+";"
				+estudiante.getTipoDocumento()+";"
				+estudiante.getNombre()+";"
				+estudiante.getApellido()+";"
				+estudiante.getPrograma()+";"
				+estudiante.getInstitucion()+";"
				+estudiante.getTipoInstitucion();

		archivoEstudiante.escribirAlFinal(registroEstudiante);
		return Const.RESULTADO_EXITOSO;
	}


}
